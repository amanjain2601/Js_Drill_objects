const mapObject = require("../mapObject.js");
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

function modifyObjectValues(value) {
    return value + 5;
}

mapObject(testObject, modifyObjectValues);

if(JSON.stringify(testObject)==JSON.stringify({ name: 'Bruce Wayne5', age: 41, location: 'Gotham5' }))
{
console.log("Result:Passed")
console.log("Modified Object",testObject);
}
else
console.log("Your mapObject function failed the test");