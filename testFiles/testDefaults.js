const defaults = require("../defaults.js");
let testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
let defaultProps = { name: 'laksh', height: "6'2", color: "fair" };

let ans = defaults(testObject, defaultProps);

if (JSON.stringify(ans) == JSON.stringify({
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham',
    height: '6\'2',
    color: 'fair'
})) {
    console.log("Result:Passed");
    console.log("Your defaults function returns", ans)
}
else
    console.log("Your defaults function failed the test");