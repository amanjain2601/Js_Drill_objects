const invert=require("../invert.js");
let testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

let ans=invert(testObject);
if(JSON.stringify(ans)==JSON.stringify({ '36': 'age', 'Bruce Wayne': 'name', Gotham: 'location' }))
{
    console.log("Result:Passed");
    console.log("Your invert functions returns",ans)
}
else
console.log("Your Invert function failed");