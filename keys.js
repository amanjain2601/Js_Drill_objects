function keys(obj) {
    let keyArray = [];
    for (let key in obj) {
        key = String(key);
        keyArray.push(key);
    }

    return keyArray;

}

module.exports = keys;