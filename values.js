function values(obj) {
    let answerArray = [];
    for (key in obj) {
        if (typeof (obj[key]) != "function")
            answerArray.push(obj[key]);
    }

    return answerArray;
}

module.exports = values;