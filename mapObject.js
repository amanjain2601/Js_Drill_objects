
function mapObject(obj, cb) {
    for (key in obj) {
        let modifiedValue = cb(obj[key]);
        obj[key] = modifiedValue;
    }

}

module.exports = mapObject;